# QuizRunner

## Opis:
Aplikacijo sestavljata dva dela in sicer osrednji del, v katerem uporabnik upravlja s figuro, ki preskakuje ovire, ki ji prihajajo naproti, in kviz izbrane teme, ki med igranjem preskakovanja ovir, na določenih mestih igralcu ponudi vprašanje, ki lahko prinese dodatne točke oz. vpliva na težavnost igre.

1.	Interaktivnost: Uporabnik upravlja z aplikacijo preko miške, kjer s klikanjem preskakuje ovire in preko tekstovnega vhoda, s katerim odgovarja na vprašanja pri kvizu.
2.	Komunikacija z oddaljenimi storitvami: Aplikacija pridobiva vprašanja in odgovore za kviz preko API-ja: https://opentdb.com/api.php 
3.	Procesiranje podatkov: Igra preskakovanja ovir, kar pomeni odzivanje figure na klike in dinamika podlage, torej premikanje ovir ter povezovanje igre s kvizom, beleženje točk ipd.

Skica strani: https://wireframe.cc/Epqwmg

![picture](preview.png)
