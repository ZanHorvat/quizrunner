--module Main exposing (..)


module BeginningElm exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode exposing (field, index, map2, string)
import Json.Encode
import Random
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time exposing (Time, millisecond, second)


type alias Question =
    String


type alias Answer =
    String


type alias Podatki =
    { vprasanje : String, odgovor : String }


type alias Model =
    { question : Question
    , answer : Answer
    , currAnswer : Answer
    , checked : Bool
    , correct : Bool
    , countCorrect : Int
    , difficulty : String
    , theme : String
    , xOvire : Int
    , yOvire : Int
    , time : Time
    , pavza : Bool
    , xFigure : Int
    , yFigure : Int
    , stevec : Int
    , vZraku : Bool
    , score : Int
    , url : String
    , countQ : Int
    , obvestilo : String
    , casZaVprasanje : Bool
    , tema : String
    , odstevalnik : Int
    , nextQ : Int
    }


type Msg
    = Check String
    | CurrAnsw String
    | NewQ (Result.Result Http.Error Podatki)
    | GetNew
    | Move Time
    | Ustavi
    | Jump
    | Hit Time
    | ChangeDifficulty String
    | ChangeTheme String
    | Q Time
    | Prazen
    | Reset String String String
    | CountDown Time
    | NQ Int


model : Model
model =
    { question = ""
    , answer = ""
    , currAnswer = ""
    , checked = False
    , correct = False
    , countCorrect = 0
    , difficulty = "easy"
    , theme = "9"
    , xOvire = 800
    , yOvire = 150
    , time = 0
    , pavza = True
    , xFigure = 215
    , yFigure = 100
    , stevec = 1
    , vZraku = False
    , score = 0
    , url = "https://opentdb.com/api.php?amount=1&difficulty=easy&type=boolean&category=9"
    , countQ = 1
    , obvestilo = "Press the start button"
    , casZaVprasanje = False
    , tema = "General Knowledge"
    , odstevalnik = 10
    , nextQ = 5
    }


view : Model -> Html Msg
view m =
    div [ Html.Attributes.class "row" ]
        [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ]
            [ div [ Html.Attributes.class "row" ]
                [ div [ Html.Attributes.class "col-lg-4 col-md-4 col-sm-4" ]
                    [ div [ Html.Attributes.class "panel panel-default" ]
                        [ div [ Html.Attributes.class "panel-body", Html.Attributes.align "center" ]
                            [ Html.button
                                [ onClick
                                    (if not m.casZaVprasanje && not (m.obvestilo == "Game over") && not (m.obvestilo == "Answer the question") then
                                        Ustavi
                                     else
                                        Prazen
                                    )
                                , Html.Attributes.style [ ( "font-size", "20px" ), ( "color", "red" ) ]
                                ]
                                [ Html.text
                                    (case ( m.casZaVprasanje, m.pavza ) of
                                        ( False, True ) ->
                                            "Start"

                                        ( _, _ ) ->
                                            "Pause"
                                    )
                                ]
                            , Html.button
                                [ onClick (Reset m.theme m.difficulty m.url)
                                , Html.Attributes.style [ ( "font-size", "20px" ), ( "color", "red" ) ]
                                ]
                                [ Html.text "Reset"
                                ]
                            ]
                        ]
                    ]
                , div [ Html.Attributes.class "col-lg-4 col-md-4 col-sm-4" ]
                    [ div [ Html.Attributes.class "panel-body", Html.Attributes.align "center" ]
                        [ b
                            [ Html.Attributes.style [ ( "font-size", "30px" ) ] ]
                            [ Html.text m.obvestilo ]
                        ]
                    ]
                , div [ Html.Attributes.class "col-lg-4 col-md-4 col-sm-4" ]
                    [ div [ Html.Attributes.class "panel panel-default" ]
                        [ div [ Html.Attributes.class "panel-body" ]
                            [ b [ Html.Attributes.style [ ( "font-size", "25px" ) ] ]
                                [ Html.text ("Result: " ++ toString m.score) ]
                            ]
                        ]
                    ]
                ]
            ]
        , div
            [ Html.Attributes.class "row" ]
            [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ]
                [ div [ Html.Attributes.class "panel panel-default" ]
                    [ div [ Html.Attributes.class "panel-body" ]
                        [ div [ Html.Attributes.class "row" ]
                            [ div [ Html.Attributes.class "col-lg-3 col-md-3 col-sm-3" ]
                                [ div [ Html.Attributes.class "panel panel-default" ]
                                    [ div [ Html.Attributes.class "panel-heading", attribute "style" "background-color: #FFA341" ]
                                        [ div [ Html.Attributes.class "row" ]
                                            [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ] [ b [] [ Html.text "Game settings: " ] ]

                                            {- !GAME LOGIC - choose difficulty -} {- !DEV CHECK - print difficulty -}
                                            ]
                                        ]
                                    , div [ Html.Attributes.class "panel-body" ]
                                        [ div [ Html.Attributes.class "row" ]
                                            [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ]
                                                [ b
                                                    []
                                                    [ Html.text "Change difficulty: " ]
                                                , Html.select [ onInput ChangeDifficulty ]
                                                    [ Html.option [ value "easy" ] [ Html.text "Easy" ]
                                                    , Html.option [ value "medium" ] [ Html.text "Medium" ]
                                                    , Html.option [ value "hard" ] [ Html.text "Hard" ]
                                                    ]
                                                , br [] []
                                                , b [] [ Html.text "Change theme: " ]
                                                , Html.select [ onInput ChangeTheme ]
                                                    [ Html.option [ value "9" ] [ Html.text "General Knowledge" ]
                                                    , Html.option [ value "20" ] [ Html.text "Mithology" ]
                                                    , Html.option [ value "21" ] [ Html.text "Sports" ]
                                                    , Html.option [ value "22" ] [ Html.text "Geography" ]
                                                    , Html.option [ value "23" ] [ Html.text "History" ]
                                                    , Html.option [ value "24" ] [ Html.text "Politics" ]
                                                    ]
                                                ]
                                            , br [] []
                                            , div [ attribute "style" "text-align:center" ]
                                                [ h1 [ Html.Attributes.style [ ( "font-size", "100px" ) ] ]
                                                    [ Html.text
                                                        (if m.casZaVprasanje then
                                                            toString m.odstevalnik
                                                         else
                                                            ""
                                                        )
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            , div [ Html.Attributes.class "col-lg-9 col-md-9 col-sm-9" ]
                                [ div [ Html.Attributes.class "panel panel-default" ]
                                    [ div [ Html.Attributes.class "panel-heading", attribute "style" "background-color: #FFA341" ]
                                        [ div [ Html.Attributes.class "row" ]
                                            [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ]
                                                [ b []
                                                    [ Html.text "Game" ]
                                                ]
                                            ]
                                        ]
                                    , div [ Html.Attributes.class "panel-body", attribute "style" "overflow: hidden; margin-right: 15px" ]
                                        [ div [ Html.Attributes.class "row" ]
                                            [ div
                                                [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12"
                                                , onClick
                                                    (if m.pavza then
                                                        Prazen
                                                     else
                                                        Jump
                                                    )
                                                ]
                                                [ svg [ Svg.Attributes.width "1000px", Svg.Attributes.height "200px", attribute "style" "background-color: #66b3ff" ]
                                                    [ image [ x (toString m.xOvire), y (toString (m.yOvire - 10)), Svg.Attributes.width "100", Svg.Attributes.height "50", xlinkHref "https://vignette.wikia.nocookie.net/fantendo/images/9/95/Warp_Pipe_NSMBW.png/revision/latest?cb=20120312005358" ] []
                                                    , rect [ x "0", y "190", Svg.Attributes.width "1000px", Svg.Attributes.height "15px", Svg.Attributes.fill "#006600" ] []
                                                    , image
                                                        [ x (toString m.xFigure)
                                                        , y (toString m.yFigure)
                                                        , Svg.Attributes.width "100"
                                                        , Svg.Attributes.height "100"
                                                        , xlinkHref
                                                            (if m.vZraku then
                                                                "https://raw.githubusercontent.com/elm-lang/debug.elm-lang.org/master/resources/imgs/mario/jump/right.gif"
                                                             else if m.pavza then
                                                                "https://raw.githubusercontent.com/elm-lang/debug.elm-lang.org/master/resources/imgs/mario/stand/right.gif"
                                                             else
                                                                "https://raw.githubusercontent.com/elm-lang/debug.elm-lang.org/master/resources/imgs/mario/walk/right.gif"
                                                            )
                                                        ]
                                                        []
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                , div [ Html.Attributes.class "panel panel-default" ]
                                    [ div [ Html.Attributes.class "panel-heading", attribute "style" "background-color: #FFA341" ]
                                        [ div [ Html.Attributes.class "row" ]
                                            [ div [ Html.Attributes.class "col-lg-12 col-md-12 col-sm-12" ]
                                                [ b []
                                                    [ Html.text "Quiz (answer true/false)" ]
                                                ]
                                            ]
                                        ]
                                    , div [ Html.Attributes.class "panel-body" ]
                                        [ div [ Html.Attributes.class "row", Html.Attributes.align "center" ]
                                            [ div
                                                [ if m.casZaVprasanje then
                                                    Html.Attributes.property "innerHTML" (Json.Encode.string m.question)
                                                  else
                                                    Html.Attributes.property "innerHTML" (Json.Encode.string "")
                                                ]
                                                []
                                            , Html.br [] []
                                            , if m.casZaVprasanje then
                                                Html.button
                                                    [ onClick
                                                        (if m.casZaVprasanje then
                                                            Check "True"
                                                         else
                                                            Prazen
                                                        )
                                                    , attribute "style" "color: green; margin: 2rem"
                                                    ]
                                                    [ Html.text
                                                        "True"
                                                    ]
                                              else
                                                Html.br [] []
                                            , if m.casZaVprasanje then
                                                Html.button
                                                    [ onClick
                                                        (if m.casZaVprasanje then
                                                            Check "False"
                                                         else
                                                            Prazen
                                                        )
                                                    , attribute "style" "color: red"
                                                    ]
                                                    [ Html.text
                                                        "False"
                                                    ]
                                              else
                                                Html.br [] []
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


newQuestion mod =
    Http.send NewQ (Http.get mod.url (field "results" (index 0 (map2 Podatki (field "question" Json.Decode.string) (field "correct_answer" Json.Decode.string)))))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg m =
    case msg of
        Check izbira ->
            if String.toLower m.answer == String.toLower izbira then
                ( { m | checked = True, odstevalnik = 10, score = m.score + ((m.countCorrect + 1) * 100), correct = True, countCorrect = m.countCorrect + 1, pavza = False, countQ = 1, casZaVprasanje = False, obvestilo = "Correct" }, Random.generate NQ (Random.int 4 8) )
            else
                ( { m | checked = True, odstevalnik = 10, correct = False, score = m.score - ((m.countCorrect + 1) * 100), pavza = False, countQ = 1, casZaVprasanje = False, obvestilo = "Wrong" }, Random.generate NQ (Random.int 4 8) )

        CurrAnsw text ->
            ( { m | currAnswer = text }, Cmd.none )

        NewQ (Ok podatki) ->
            ( { m | checked = False, correct = False, question = podatki.vprasanje, answer = podatki.odgovor, casZaVprasanje = True }, Cmd.none )

        NewQ (Err _) ->
            ( { m | checked = False, correct = False, question = "API is down, please try later ...", answer = "" }, Cmd.none )

        GetNew ->
            ( m, newQuestion m )

        Move newTime ->
            case ( m.xOvire, m.vZraku ) of
                ( 0, True ) ->
                    if m.stevec % 15 == 0 then
                        ( { m | xOvire = 800, score = m.score + 1, stevec = m.stevec + 1, yFigure = 100, vZraku = False }, Cmd.none )
                    else
                        ( { m | xOvire = 800, score = m.score + 1, stevec = m.stevec + 1 }, Cmd.none )

                ( 0, False ) ->
                    ( { m | xOvire = 800, score = m.score + 1 }, Cmd.none )

                ( _, True ) ->
                    if m.stevec % 15 == 0 then
                        ( { m | xOvire = m.xOvire - 20, score = m.score + 1, stevec = m.stevec + 1, yFigure = 100, vZraku = False }, Cmd.none )
                    else
                        ( { m | xOvire = m.xOvire - 20, score = m.score + 1, stevec = m.stevec + 1 }, Cmd.none )

                ( _, _ ) ->
                    ( { m | xOvire = m.xOvire - 20, score = m.score + 1 }, Cmd.none )

        Ustavi ->
            if m.pavza then
                ( { m | pavza = False, obvestilo = "" }, Cmd.none )
            else
                ( { m | pavza = True }, Cmd.none )

        Jump ->
            ( { m | yFigure = 30, vZraku = True, countQ = m.countQ + 1 }, Cmd.none )

        Hit newTime ->
            ( { m | pavza = True, obvestilo = "Game over" }, Cmd.none )

        ChangeDifficulty option ->
            ( { m | difficulty = option, url = "https://opentdb.com/api.php?amount=1&difficulty=" ++ option ++ "&type=boolean&category=" ++ m.theme }, Cmd.none )

        ChangeTheme option ->
            ( { m
                | tema =
                    if option == "9" then
                        "General Knowledge"
                    else if option == "20" then
                        "Mithology"
                    else if option == "21" then
                        "Sports"
                    else if option == "22" then
                        "Geography"
                    else if option == "23" then
                        "History"
                    else
                        "Politics"
                , theme = option
                , url = "https://opentdb.com/api.php?amount=1&difficulty=" ++ m.difficulty ++ "&type=boolean&category=" ++ option
              }
            , Cmd.none
            )

        NQ n ->
            ( { m | nextQ = n }, Cmd.none )

        Q newTime ->
            ( { m | pavza = True, obvestilo = "Answer the question" }, newQuestion m )

        -- to stanje klicejo gumbi v casu ko se jih ne sme pritiskati, torej neka crna luknja
        Prazen ->
            ( { m | stevec = m.stevec + 1 }, Cmd.none )

        Reset x y z ->
            ( { model | theme = x, difficulty = y, url = z }, Cmd.none )

        CountDown newTime ->
            if m.odstevalnik > 0 then
                ( { m | odstevalnik = m.odstevalnik - 1 }, Cmd.none )
            else
                ( { m | checked = True, correct = False, score = m.score - 100, pavza = False, countQ = 1, casZaVprasanje = False, obvestilo = "Sorry, too late", odstevalnik = 10 }, Random.generate NQ (Random.int 4 8) )


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.pavza && not model.casZaVprasanje then
        Sub.none
    else if model.pavza && model.casZaVprasanje then
        Time.every second CountDown
    else if Basics.abs (model.xOvire - model.xFigure) < 45 && model.vZraku == False then
        Time.every (50 * millisecond) Hit
    else if model.countQ % model.nextQ == 0 then
        Time.every (10 * millisecond) Q
    else
        Time.every (toFloat (randomInt model.yOvire) * millisecond) Move


randomInt : Int -> Int
randomInt seed =
    (1103515426 * seed + 12345) % 100


main =
    Html.program { init = ( model, Cmd.none ), view = view, update = update, subscriptions = subscriptions }
